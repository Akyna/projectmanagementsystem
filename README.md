# Project Management System  #

The Project Management System creates a list of tasks, issuances tasks for developers, changes the status of the task, and deletes projects and tasks.

### Technology ###
* JDK 1.8
* EJB
* JPA
* Generic DAO
* REST
* PostgreSQL
* WildFly
* Maven 3.3.3
* HTML/CSS