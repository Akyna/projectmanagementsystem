package akyna.java.rest;

import akyna.java.model.Task;
import akyna.java.service.TaskService;

import javax.ejb.EJB;
import javax.ws.rs.*;

@Path("/task")
public class TaskResource {

    @EJB
    private TaskService taskService;

    @GET
    @Path("/{taskId}")
    @Produces("text/json")
    public Task read(@PathParam("taskId") Long taskId) {
        return taskService.read(taskId);

    }

    @GET
    @Path("/xml/{taskId}")
    @Produces("text/xml")
    public Task readAsXML(@PathParam("taskId") Long taskId) {
        return taskService.read(taskId);
    }

    @PUT
    @Produces("text/json")
    @Consumes("text/json")
    public Long read(Task task) {
        taskService.create(task);
        return task.getId();
    }

    @DELETE
    @Path("/del/{taskId}")
    public String delete(@PathParam("taskId") Long taskId) {
        taskService.delete(taskId);
        return "Task "+ taskId + " deleted";
    }

}
