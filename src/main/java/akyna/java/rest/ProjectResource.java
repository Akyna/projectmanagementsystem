package akyna.java.rest;

import akyna.java.model.Project;
import akyna.java.service.ProjectService;

import javax.ejb.EJB;
import javax.ws.rs.*;

@Path("/project")
public class ProjectResource {

    @EJB
    private ProjectService projectService;

    @GET
    @Path("/{projectId}")
    @Produces("text/json")
    public Project read(@PathParam("projectId") Long projectId) {
        return projectService.read(projectId);

    }

    @GET
    @Path("/xml/{projectId}")
    @Produces("text/xml")
    public Project readAsXML(@PathParam("projectId") Long projectId) {
        return projectService.read(projectId);
    }

    @PUT
    @Produces("text/json")
    @Consumes("text/json")
    public Long read(Project project) {
        projectService.create(project);
        return project.getId();
    }

    @DELETE
    @Path("/del/{projectId}")
    public String delete(@PathParam("projectId") Long projectId) {
        projectService.delete(projectId);
        return "Developer "+ projectId + " deleted";
    }

}
