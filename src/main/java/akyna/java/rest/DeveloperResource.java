package akyna.java.rest;

import akyna.java.model.Developer;
import akyna.java.service.DeveloperService;

import javax.ejb.EJB;
import javax.ws.rs.*;

@Path("/developer")
public class DeveloperResource {

    @EJB
    private DeveloperService developerService;

    @GET
    @Path("/{developerId}")
    @Produces("text/json")
    public Developer read(@PathParam("developerId") Long developerId) {
        return developerService.read(developerId);

    }

    @GET
    @Path("/xml/{developerId}")
    @Produces("text/xml")
    public Developer readAsXML(@PathParam("developerId") Long developerId) {
        return developerService.read(developerId);
    }

    @PUT
    @Produces("text/json")
    @Consumes("text/json")
    public Long read(Developer developer) {
        developerService.create(developer);
        return developer.getId();
    }

    @DELETE
    @Path("/del/{developerId}")
    public String delete(@PathParam("developerId") Long developerId) {
        developerService.delete(developerId);
        return "Developer "+ developerId + " deleted";
    }

}
