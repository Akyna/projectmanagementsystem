package akyna.java.rest.response.json;

public class ManagerNotFoundResponse {
    private String status;

    public ManagerNotFoundResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
