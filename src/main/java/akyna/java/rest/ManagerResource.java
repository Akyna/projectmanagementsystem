package akyna.java.rest;

import akyna.java.exception.ManagerNotFoundException;
import akyna.java.model.Manager;
import akyna.java.rest.response.json.ManagerNotFoundResponse;
import akyna.java.service.ManagerService;

import javax.ejb.EJB;
import javax.ws.rs.*;

@Path("/manager")
public class ManagerResource {

    @EJB
    private ManagerService managerService;

    @GET
    @Path("/{managerId}")
    @Produces("text/json")
    public Manager read(@PathParam("managerId") Long managerId) {
//        return managerService.readManagerWithProject(managerId);
        Manager manager = managerService.read(managerId);
        if (manager == null) {
            throw new ManagerNotFoundException(new ManagerNotFoundResponse("Manager not Found"));
        }
        return manager;

    }

    @GET
    @Path("/xml/{managerId}")
    @Produces("text/xml")
    public Manager readAsXML(@PathParam("managerId") Long managerId) {
//        return managerService.readManagerWithProject(managerId);
        return managerService.read(managerId);

    }


    @PUT
    @Produces("text/json")
    @Consumes("text/json")
    public Long read(Manager manager) {
        managerService.create(manager);
        return manager.getId();
    }

    @DELETE
    @Path("/del/{managerId}")
    public String delete(@PathParam("managerId") Long managerId) {
        managerService.delete(managerId);
        return "Manager "+ managerId + " deleted";
    }

}
