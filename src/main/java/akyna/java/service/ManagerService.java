package akyna.java.service;

import akyna.java.dao.ManagerDao;
import akyna.java.model.Employee;
import akyna.java.model.Manager;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class ManagerService {

    @EJB
    private ManagerDao managerDao;

    public Manager read(long id) {
        return managerDao.read(id);
    }


    public void createInitialManager() {
        if (managerDao.readAll().isEmpty()) {
            Manager defaultManager = new Manager();
            defaultManager.setName("Bob");
            defaultManager.setSurname("Marley");
            defaultManager.setEmail("q@a.com");
            defaultManager.setPosition(Employee.EmployeePosition.MANAGER);

            managerDao.create(defaultManager);
        }
    }

    public Manager readManagerWithProject(Long managerId) {
        Manager manager = managerDao.read(managerId);
        manager.getProjects().size();
        return manager;
    }

    public void create(Manager manager) {
        managerDao.create(manager);
    }

    public void delete(Long managerId) {
        managerDao.delete(managerDao.read(managerId));
    }
}
