package akyna.java.service;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class Bootstrap {
    @EJB
    private ManagerService managerService;

    @EJB
    private DeveloperService developerService;

    @PostConstruct
    public void init() {
        managerService.createInitialManager();
        developerService.createInitialDevelopers();
    }

}
