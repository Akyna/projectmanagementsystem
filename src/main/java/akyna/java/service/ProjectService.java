package akyna.java.service;

import akyna.java.dao.ProjectDao;
import akyna.java.exception.MyException;
import akyna.java.model.Project;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collection;

@Stateless
public class ProjectService {

    @EJB
    private ProjectDao projectDao;

    public void create(Project project) {
        projectDao.create(project);
    }

    public Project read(Long projectId) {
        return projectDao.read(projectId);
    }

    public void update(Project project) {
        projectDao.update(project);
    }

    public void delete(Project project) throws MyException {
        Project projectToDelete = projectDao.read(project.getId());

        if (projectToDelete.getTasks().isEmpty()) {
            projectDao.delete(projectToDelete);
        } else {
            throw new MyException("Cannot delete");
        }

    }

    public Collection<Project> readAll() {
        return projectDao.readAll();
    }

    public void delete(Long projectId) {
        projectDao.delete(projectDao.read(projectId));
    }
}
