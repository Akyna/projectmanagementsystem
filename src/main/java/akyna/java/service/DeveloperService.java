package akyna.java.service;


import akyna.java.dao.DeveloperDao;
import akyna.java.model.Developer;
import akyna.java.model.Employee;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collection;

@Stateless
public class DeveloperService {

    @EJB
    private DeveloperDao developerDao;

    public Collection<Developer> readAll() {
        return developerDao.readAll();
    }

    public void createInitialDevelopers() {
        if (readAll().isEmpty()) {
            for (int i = 0; i < 3; i++) {
                Developer developer = new Developer();
                developer.setName("Bob_" + i);
                developer.setSurname("Marley_" + i);
                developer.setEmail("q@a.com_" + i);
                developer.setPosition(Employee.EmployeePosition.DEVELOPER);
                developerDao.create(developer);
            }
        }
    }

    public Developer read(Long developerId) {
        return developerDao.read(developerId);
    }

    public void create(Developer developer) {
        developerDao.create(developer);
    }

    public void delete(Long developerId) {
        developerDao.delete(developerDao.read(developerId));
    }
}
