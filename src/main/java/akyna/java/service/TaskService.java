package akyna.java.service;

import akyna.java.dao.TaskDao;
import akyna.java.model.Developer;
import akyna.java.model.Task;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collection;

@Stateless
public class TaskService {

    @EJB
    private TaskDao taskDao;

    public void create(Task task) {
        taskDao.create(task);
    }

    public Task read(Long taskId) {
        return taskDao.read(taskId);
    }

    public void update(Task task) {
        taskDao.update(task);
    }

    public void delete(Task task) {
        taskDao.delete(task);
    }

    public Collection<Task> readAll() {
        return taskDao.readAll();
    }

    public Collection<Task> readTaskInProject(Long projectId) {
        return taskDao.getTaskByProject(projectId);
    }

    public void updateTask(Task task, Developer developer) {
        task.setDeveloper(developer);
        taskDao.update(task);
    }

    public void delete(Long taskId) {
        taskDao.delete(taskDao.read(taskId));
    }
}
