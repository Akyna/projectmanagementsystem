package akyna.java.dao;

import akyna.java.model.Manager;

public interface ManagerDao extends GenericDao<Manager> {
}
