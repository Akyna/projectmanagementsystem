package akyna.java.dao;

import akyna.java.model.Task;

import java.util.List;

public interface TaskDao extends GenericDao<Task> {
    List<Task> getTaskByProject(Long projectId);
}
