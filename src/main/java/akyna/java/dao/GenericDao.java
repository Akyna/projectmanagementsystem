package akyna.java.dao;

import java.util.Collection;

public interface GenericDao<T> {
    void create(T entity);

    T read(Long id);

    void update(T entity);

    void delete(T entity);

    Collection<T> readAll();
}