package akyna.java.dao.jpa;

import akyna.java.dao.DeveloperDao;
import akyna.java.model.Developer;

import javax.ejb.Stateless;

@Stateless
public class JpaDeveloperDao extends JpaGenericDao<Developer> implements DeveloperDao {

}
