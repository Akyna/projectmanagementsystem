package akyna.java.dao.jpa;

import akyna.java.dao.ManagerDao;
import akyna.java.model.Manager;

import javax.ejb.Stateless;
@Stateless
public class JpaManagerDao extends JpaGenericDao<Manager> implements ManagerDao {

}
