package akyna.java.dao.jpa;


import akyna.java.dao.ProjectDao;
import akyna.java.model.Project;

import javax.ejb.Stateless;

@Stateless
public class JpaProjectDao extends JpaGenericDao<Project> implements ProjectDao {
}
