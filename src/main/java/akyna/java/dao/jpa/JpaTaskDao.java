package akyna.java.dao.jpa;

import akyna.java.dao.TaskDao;
import akyna.java.model.Task;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class JpaTaskDao extends JpaGenericDao<Task> implements TaskDao {

    private static final String GET_TASK_BY_PROJECTI_QUERY =
            "SELECT e " +
                    "FROM Task e " +
                    "WHERE e.project.id = :projectId";

    @Override
    public List<Task> getTaskByProject(Long projectId) {
        List<Task> tasks = em.createQuery(GET_TASK_BY_PROJECTI_QUERY)
                .setParameter("projectId", projectId)
                .getResultList();

        return tasks;
    }
}
