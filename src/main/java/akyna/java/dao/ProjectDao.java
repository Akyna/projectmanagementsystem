package akyna.java.dao;

import akyna.java.model.Project;


public interface ProjectDao extends GenericDao<Project> {
}
