package akyna.java.dao;

import akyna.java.model.Developer;

public interface DeveloperDao extends GenericDao<Developer> {
}
