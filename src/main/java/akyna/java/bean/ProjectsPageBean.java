package akyna.java.bean;

import akyna.java.exception.MyException;
import akyna.java.model.Project;
import akyna.java.service.ProjectService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Collection;

@ViewScoped
@ManagedBean(name = "projects")
public class ProjectsPageBean implements Serializable {
    private Collection<Project> projects;

    @EJB
    private ProjectService projectService;

    @PostConstruct
    public void init() {
        projects = projectService.readAll();
    }

    public void deleteProject(Project project) {

        try {
            projectService.delete(project);
        } catch (MyException e) {
            FacesMessage msg = new FacesMessage("Невозможно удалить проект, так как он имеет задачи");
            msg.setSeverity(FacesMessage.SEVERITY_WARN);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        projects = projectService.readAll();
    }

    public Collection<Project> getProjects() {
        return projects;
    }

}
