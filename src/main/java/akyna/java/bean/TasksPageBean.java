package akyna.java.bean;

import akyna.java.model.Task;
import akyna.java.service.TaskService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Collection;

@ViewScoped
@ManagedBean(name = "tasks")
public class TasksPageBean implements Serializable{
    private Collection<Task> tasks;

    @EJB
    private TaskService taskService;

    @PostConstruct
    public void init() {
        tasks = taskService.readTaskInProject(retrieveProjectId());
    }

    private Long retrieveProjectId() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        return Long.valueOf(ec.getRequestParameterMap().get("projectId"));
    }

    public void deleteTask(Task task) {

        taskService.delete(task);
        tasks = taskService.readTaskInProject(retrieveProjectId());
    }

    public Collection<Task> getTasks() {
        return tasks;
    }
}
