package akyna.java.bean;

import akyna.java.model.Developer;
import akyna.java.model.Project;
import akyna.java.model.Task;
import akyna.java.service.DeveloperService;
import akyna.java.service.ProjectService;
import akyna.java.service.TaskService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.util.Collection;

@ViewScoped
@ManagedBean(name = "editTask")
public class EditTaskPageBean {
    private Task task;
    private Collection<Developer> developers;
    private Developer developer;
    private Project project;


    @EJB
    private DeveloperService developerService;
    @EJB
    private TaskService taskService;
    @EJB
    private ProjectService projectService;

    @PostConstruct
    public void init() {
        developers = developerService.readAll();
        task = taskService.read(retrieveTaskId());
        project = projectService.read(retrieveProjectId());
    }

    private Long retrieveProjectId() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        return Long.valueOf(ec.getRequestParameterMap().get("projectId"));
    }


    private Long retrieveTaskId() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        return Long.valueOf(ec.getRequestParameterMap().get("taskId"));
    }

    public String updateTask() {
        taskService.updateTask(task, developer);
        return "project-tasks?faces-redirect=true&projectId="+project.getId();
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Collection<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(Collection<Developer> developers) {
        this.developers = developers;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }
}
