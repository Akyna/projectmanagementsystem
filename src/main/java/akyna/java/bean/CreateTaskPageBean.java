package akyna.java.bean;

import akyna.java.model.Developer;
import akyna.java.model.Project;
import akyna.java.model.Task;
import akyna.java.service.DeveloperService;
import akyna.java.service.ProjectService;
import akyna.java.service.TaskService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Collection;

@ViewScoped
@ManagedBean(name = "createTask")
public class CreateTaskPageBean implements Serializable {
    private String title;
    private String description;
    private Project project;
    private Collection<Developer> developers;
    private Developer developer;

    @EJB
    private TaskService taskService;
    @EJB
    private DeveloperService developerService;
    @EJB
    private ProjectService projectService;

    @PostConstruct
    public void init() {
        project = projectService.read(retrieveProjectId());
        developers = developerService.readAll();
    }

    private Long retrieveProjectId() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        return Long.valueOf(ec.getRequestParameterMap().get("projectId"));
    }

    public String createTask() {
        try {
            taskService.create(new Task(title, description, project, developer));
            return "project-tasks?faces-redirect=true&projectId=" + project.getId();
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error", e.getMessage()));
            return null;
        }
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Collection<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(Collection<Developer> developers) {
        this.developers = developers;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }
}
