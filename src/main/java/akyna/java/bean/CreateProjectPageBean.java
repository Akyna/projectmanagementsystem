package akyna.java.bean;

import akyna.java.model.Manager;
import akyna.java.model.Project;
import akyna.java.service.ManagerService;
import akyna.java.service.ProjectService;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;

@RequestScoped
@ManagedBean(name = "createProject")
public class CreateProjectPageBean implements Serializable {
    private String title;

    @EJB
    private ProjectService projectService;

    @EJB
    private ManagerService managerService;

    public String createProject() {
        Project project = new Project(title);
        Manager manager = managerService.read(1L);

        project.setManager(manager);

        projectService.update(project);
        return "projects?faces-redirect=true";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
