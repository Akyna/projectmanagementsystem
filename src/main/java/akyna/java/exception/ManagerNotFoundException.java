package akyna.java.exception;

import akyna.java.rest.response.json.ManagerNotFoundResponse;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class ManagerNotFoundException extends WebApplicationException {
    public ManagerNotFoundException(ManagerNotFoundResponse jsonObject) {
        super(Response.status(Response.Status.NOT_FOUND).entity(jsonObject).type("text/json").build());
    }

}
