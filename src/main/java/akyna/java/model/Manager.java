package akyna.java.model;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
@Entity
@Table(name = "manager")
public class Manager extends Employee implements Serializable {

    @XmlTransient
    @OneToMany(mappedBy = "manager")
    private List<Project> projects;


    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}
