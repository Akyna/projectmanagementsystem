package akyna.java.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Entity
@Table(name = "task")
public class Task implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column
    private String title;
    @Column
    private String description;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private TaskStatus taskStatus = TaskStatus.NEW;

    @ManyToOne
    @JsonIgnore
    @XmlTransient
    @JoinColumn(name = "project_id")
    private Project project;

    @ManyToOne
    @JsonIgnore
    @XmlTransient
    @JoinColumn(name = "developer_id")
    private Developer developer;


    public Task() {
    }

    public Task(String title, String description) {
        this.title = title;
        this.description = description;
    }


    public Task(String title, String description, Project project, Developer developer) {
        this.title = title;
        this.description = description;
        this.project = project;
        this.developer = developer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }


    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(title, task.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }


    public enum TaskStatus {
        NEW(1), IN_PROGRESS(2), IMPLEMENTED(3);
        private int code;

        TaskStatus(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
}
